# **Wireframing**

![mockflow](mockflow-logo.png)  **Voir un exemple de WireFrame pour la création d'un jeu en HTML - CSS - JavaScript avec [mockflow](https://www.mockflow.com/) : [Pass The Pigs](http://thierry-sautiere.000webhostapp.com/WireFraming/)**

![mockflow](mockflow-logo.png) **Vidéo d'utilisation de MockFlow : [![video](mockFlowQRCODE.png)](https://www.youtube.com/watch?v=h3Ldp8qKggE)**


## ![projet](gestiondeprojet.png) **Projet d'évaluation : Réalisation du WireFraming de sa page CV**

**Quelques exemples de CV pour activer votre inspiration !**

![CV1](cv1.PNG)![CV2](cv2.PNG)![CV3](cv3.PNG)